#include "freertos/FreeRTOS.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_event_loop.h"
#include "esp_partition.h"
#include "esp_err.h"
// #include "driver/spi_master.h"
#include "esp_log.h"
#include "esp_vfs_fat.h"
// #include "driver/sdmmc_host.h"
#include "driver/sdspi_host.h"
#include "sdmmc_cmd.h"
#include "esp_spiffs.h"
#include "nvs_flash.h"
#include "driver/gpio.h"
#include "psxcontroller.h"
#include "nofrendo.h"
#include "menu.h"

#define READ_BUFFER_SIZE 64
#define ASSERT_ESP_OK(returnCode, message)                          \
	if (returnCode != ESP_OK)                                       \
	{                                                               \
		printf("%s. (%s)\n", message, esp_err_to_name(returnCode)); \
		return returnCode;                                          \
	}

char *selectedRomFilename;	// ROM file name


char *nes_100ask_osd_getromdata()
{
	char *romdata;
	spi_flash_mmap_handle_t handle;
	esp_err_t err;

	// Locate our target ROM partition where the file will be loaded
	esp_partition_t *part = esp_partition_find_first(ESP_PARTITION_TYPE_DATA, 0xFF, "rom");
	if (part == 0)
		printf("Couldn't find rom partition!\n");

	// Open the file
	printf("Reading rom from %s\n", selectedRomFilename);
	FILE *rom = fopen(selectedRomFilename, "r");
	long fileSize = -1;
	if (!rom)
	{
		printf("Could not read %s\n", selectedRomFilename);
		exit(1);
	}

	// First figure out how large the file is
	fseek(rom, 0L, SEEK_END);
	fileSize = ftell(rom);

	rewind(rom);
	if (fileSize > part->size)
	{
		printf("Rom is too large!  Limit is %dk; Rom file size is %dkb\n",
			   (int)(part->size / 1024),
			   (int)(fileSize / 1024));
		exit(1);
	}

	// Copy the file contents into EEPROM memory
	char buffer[READ_BUFFER_SIZE];
	int offset = 0;
	while (fread(buffer, 1, READ_BUFFER_SIZE, rom) > 0)
	{
		if ((offset % 4096) == 0)
			esp_partition_erase_range(part, offset, 4096);
		esp_partition_write(part, offset, buffer, READ_BUFFER_SIZE);
		offset += READ_BUFFER_SIZE;
	}
	fclose(rom);

	printf("Loaded %d bytes into ROM memory\n", offset);
 
	// Memory-map the ROM partition, which results in 'data' pointer changing to memory-mapped location
	err = esp_partition_mmap(part, 0, fileSize, SPI_FLASH_MMAP_DATA, (const void **)&romdata, &handle);
	if (err != ESP_OK)
		printf("Couldn't map rom partition!\n");
	printf("Initialized. ROM@%p\n", romdata);

	return (char *)romdata;
}

void esp_wake_deep_sleep()
{
	esp_restart();
}

esp_err_t nes_100ask_event_handler(void *ctx, system_event_t *event)
{
	return ESP_OK;
}

esp_err_t nes_100ask_register_sd_card()
{
	esp_err_t ret;
	sdmmc_card_t *card;
	sdmmc_host_t host = SDSPI_HOST_DEFAULT();
	host.command_timeout_ms=200;
	//host.flags = SDMMC_HOST_FLAG_4BIT;
	host.max_freq_khz = SDMMC_FREQ_PROBING;
	//host.max_freq_khz = 240;

	sdspi_slot_config_t slot_config = SDSPI_SLOT_CONFIG_DEFAULT();
	slot_config.gpio_miso =  CONFIG_SD_MISO;
	slot_config.gpio_mosi =  CONFIG_SD_MOSI;
	slot_config.gpio_sck =   CONFIG_SD_SCK;
	slot_config.gpio_cs =    CONFIG_SD_CS;
	slot_config.dma_channel = 2;
	esp_vfs_fat_sdmmc_mount_config_t mount_config = {
		.format_if_mount_failed = false,
		.max_files = 5,
		//.allocation_unit_size = 16 * 1024
		};

	ret = esp_vfs_fat_sdmmc_mount("/sdcard", &host, &slot_config, &mount_config, &card);
	ASSERT_ESP_OK(ret, "Failed to mount SD card");

    // Card has been initialized, print its properties
    sdmmc_card_print_info(stdout, card);
	
	return ESP_OK;
}

int app_main(void)
{
	printf("100ASK NESEMU START!\n");

	ASSERT_ESP_OK(nes_100ask_register_sd_card(), "Unable to register SD Card");	// 初始化存储设备(SD卡)

	nes_100ask_controller_init();				// 初始化输入设备
	selectedRomFilename = nes_100ask_run_menu();

	nofrendo_main(0, NULL);			// 进入nes模拟器循环
	printf("NoFrendo died? WtF?\n");
	asm("break.n 1");
	return 0;
}
