#pragma once
#include <stdint.h>
#include "esp_err.h"

/**
 * @brief running intro and menu (choose rom/game)
 *
 * @return - integer for choosing game partition
 */
#define ROM_LIST "/sdcard/roms.txt"
//#define ROM_LIST "/spiffs/roms.txt"
//#define ROM_LIST "/roms.txt"
#define FILENAME_LENGTH 32//18

char* nes_100ask_run_menu();
void nes_100ask_set_br(int bright);
void freeMenuResources();

typedef struct menuEntry
{
	//int entryNumber;
	char icon;
	//char name[FILENAME_LENGTH+1];
	char fileName[FILENAME_LENGTH+1];
} MenuEntry;
int entryCount;
MenuEntry *menuEntries;
