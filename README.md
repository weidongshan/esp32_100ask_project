# 例程说明
百问网NES游戏机项目功能强大，硬件组合合理，成本低、可DIY性高，模块化的硬件可重复利用到其他项目或者实验，本项目功能特点：

- 通过 [SPI sd卡模块](https://item.taobao.com/item.htm?id=642291784020) 从外挂的micro sd 卡(SPI)中加载游戏文件(Fat文件系统)
- [2.2寸ILI9341显示屏](https://item.taobao.com/item.htm?id=642291784020) (SPI接口240x320分辨率)显示游戏图象
- 支持开机动画、菜单选择游戏运行
- 支持音频输出(I2S)，使用 [CJMCUTRRS 3.5mm音频座模块](https://item.taobao.com/item.htm?id=642291784020) 输出音频更方便！
- 支持PCF8574 I/O拓展模块(I2C接口)控制游戏，拓展的8个I/O引脚用于游戏按键(上、下、左、右、A、B、select、start)
- 支持使用国内 [7针、9针NES游戏手柄](https://item.taobao.com/item.htm?id=642291784020) 控制游戏
- 支持国内 [MicroUSB接口的游戏手柄](https://item.taobao.com/item.htm?id=642291784020) 控制游戏

> 项目使用 VScode+platformio插件开发，以上功能均可在 `./platformio.ini` 文件裁剪。
> 请将NES游戏文件存放在sd卡中的 `/nes` 文件夹。
> 该项目同时移植到了 raspberry pi pico 上，视频教程：[raspberry pi pico|【项目一】找回童年！Raspberry pi(树莓派) pico NES模拟器(树莓派pico(单片机) NES游戏机)](https://www.bilibili.com/video/BV1UB4y1P7iU)

# 例程使用的开发板

[ESP32-DevKitC V4](https://item.taobao.com/item.htm?id=642291784020)


## 开发板介绍
搭载全新系统级芯片的ESP32-DevKitC开发板为开发者带来福音。
ESP32-DevKitC是搭载了乐鑫全新的ESP32-WROOM-32模组的MINI开发板，能够轻松地插接到面包板。

### ESP32-DevKitC亮点

- 新颖强大。这款小型系统开发板搭载了乐鑫全新的系统级芯片ESP32，支持Wi-Fi和蓝牙功能，具有丰富的外设，能够让开发者尽情发挥想象力进行二次开发！
- 开发方便迅速。ESP32-DevKitC的射频性能已经调试完善，用户进行应用设计和开发时无需考虑射频和天线设计。此开发板包含了用户所需的小型系统，只需连上USB线，即可进行开发。
- 特性灵活丰富。ESP32-DevKitC 具备支持ESP32-WROOM-32的完整电路，包括USB-UART转换器，复位和下载模式按钮，LDO稳压器和微型USB连接器。每个GPIO都可供开发者使用。
- 面包板插接方便。ESP32-DevKitC 引脚分布优化，能够方便地插接到面包板进行开发和调试。板载LDO被引出，可为外部元器件供电。不同的外设接口进行分组，从而实现无障碍开发。
- 亚马逊认证设备。乐鑫ESP32-DevKitC开发板已正式通过Amazon AWS的资格认证计划，可兼容Amazon FreeRTOS操作系统。这也就是说，除了乐鑫的原生ESP-IDFSDK，您还可以通过Amazon FreeRTOS，轻松连接AWS loT、AWS Greengrass及更多丰富AWS服务。

## 开发板模组介绍

ESP32-WROOM-32D是乐鑫通用型Wi-Fi+BT+BLE MCU 模组，功能强大，用途广泛，可以用于低功耗传感器网络和要求极高的任务，例如语音编码、音频流和MP3 解码等。

ESP32-WROOM-32D 模组的核心是ESP32-D0WD 芯片，该款芯片属于ESP32系列，具有可扩展、自适应的特点。
两个CPU 核可以被单独控制。时钟频率的调节范围为80 MHz 到240MHz。
用户可以切断CPU 的电源，利用低功耗协处理器来不断地监测外设的状态变化或某些模拟量是否超出阈值。
ESP32 还集成了丰富的外设，包括电容式触摸传感器、霍尔传感器，SD 卡接口、以太网接口、高速SDIO/SPI、UART、I²S 和I²C 等。

模组集成了传统蓝牙、低功耗蓝牙和Wi-Fi，具有广泛的用途：Wi-Fi 支持极大范围的通信连接，也支持通过路由器直接连接互联网；而蓝牙可以让用户连接手机或者广播BLE Beacon 以便于信号检测。ESP32 芯片的睡眠电流小于5 μA，使其适用于电池供电的可穿戴电子设备。
模组支持的数据传输速率高达150 Mbps，天线输出功率达到20 dBm，可实现最大范围的无线通信。
因此，这款模组具有行业领先的技术规格，在高集成度、无线传输距离、功耗以及网络联通等方面性能极佳。
ESP32 的操作系统是带有LwIP 的freeRTOS，还内置了带有硬件加速功能的TLS 1.2。
芯片同时支持OTA 加密升级，方便用户在产品发布之后继续升级。

### 模组参数
| | |
| --- | --- |
| 模组 | ESP32-WROOM-32D  |
| 芯片 | ESP32-D0WD  |
| SPI Flash | 32 Mbit, 3.3 V  |
| 晶振 | 40 MHz  |
| 天线 | 板载天线  |
| 模组尺寸(mm) | (18.00±0.10) × (25.50±0.10) × (3.10±0.10)  |

# 开发板及配套模块链接

[https://item.taobao.com/item.htm?id=642291784020](https://item.taobao.com/item.htm?id=642291784020)


# 视频教程

- 百问网官网: [https://www.100ask.net/](https://www.100ask.net/)
- Bilibili观看：[https://www.bilibili.com/video/BV1ef4y1W7En](https://www.bilibili.com/video/BV1ef4y1W7En)

