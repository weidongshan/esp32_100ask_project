#pragma once
#include <stdint.h>
#include "esp_err.h"


/**
 * @brief Calculate and disaply a set of rows to the screen
 *
 * @param dest Destination for the pixels. Assumed to be LINECT * 320 16-bit pixel values.
 * @param y Starting y coordinate of the chunk of lines.
 * @param rowCount Amount of lines to calculate
 */
void nes_100ask_draw_rows(uint16_t *dest, int y, int rowCount);

void nes_100ask_handle_user_input();

/**
 * @brief Initialize the boot screen and menu
 *
 * @return ESP_OK on success, an error from the jpeg decoder otherwise.
 */
esp_err_t nes_100ask_menu_init();

bool nes_100ask_pe_get_pixel(char peChar, int pe1, int pe2);

void nes_100ask_set_line_max(int lineM);

int nes_100ask_get_sel_rom();

void nes_100ask_set_sel_rom(int selR);

void nes_100ask_free_mem();

bool nes_100ask_get_y_stretch();

bool nes_100ask_get_x_stretch();

void nes_100ask_set_x_stretch(bool str);

void nes_100ask_set_y_stretch(bool str);

void nes_100ask_set_bright(int bright);

void initGPIO(int gpioNo);
