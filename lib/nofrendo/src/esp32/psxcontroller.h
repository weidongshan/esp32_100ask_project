#ifndef PSXCONTROLLER_H
#define PSXCONTROLLER_H

int nes_100ask_controller_read_input();
void nes_100ask_controller_init();
bool getShowMenu();
int nes_100ask_get_bright();
int nes_100ask_get_volume();
bool nes_100ask_get_shutdown();
bool nes_100ask_is_select_pressed(int ctl);
bool nes_100ask_is_start_pressed(int ctl);
bool nes_100ask_is_up_pressed(int ctl);
bool nes_100ask_is_right_pressed(int ctl);
bool nes_100ask_is_down_pressed(int ctl);
bool nes_100ask_is_left_pressed(int ctl);
bool nes_100ask_is_a_pressed(int ctl);
bool nes_100ask_is_b_pressed(int ctl);
bool nes_100ask_is_turbo_a_pressed(int ctl);
bool nes_100ask_is_turbo_b_pressed(int ctl);
bool nes_100ask_is_menu_pressed(int ctl);
bool nes_100ask_is_any_power_pressed(int ctl);
bool is_any_pressed(int ctl);
bool nes_100ask_is_any_fire_pressed(int ctl);
int nes_100ask_get_turbo_a();
int nes_100ask_get_turbo_b();

#endif