// Copyright 2015-2016 Espressif Systems (Shanghai) PTE LTD
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"

#include "driver/gpio.h"
#include "soc/gpio_struct.h"
#include "psxcontroller.h"
#include "sdkconfig.h"
#include "pretty_effect.h"
#include "esp_deep_sleep.h"
//#include "esp_sleep.h"
#include "driver/i2c.h"


#define I2C_MASTER_TX_BUF_DISABLE 0 /*!< I2C master doesn't need buffer */
#define I2C_MASTER_RX_BUF_DISABLE 0 /*!< I2C master doesn't need buffer */
#define WRITE_BIT I2C_MASTER_WRITE  /*!< I2C master write */
#define READ_BIT I2C_MASTER_READ    /*!< I2C master read */
#define ACK_CHECK_EN 0x1            /*!< I2C master will check ack from slave*/
#define ACK_CHECK_DIS 0x0           /*!< I2C master will not check ack from slave */
#define ACK_VAL 0x0                 /*!< I2C ack value */
#define NACK_VAL 0x1                /*!< I2C nack value */
static uint32_t i2c_frequency = 100000;
static i2c_port_t i2c_port = I2C_NUM_0;


#define DELAY() asm("nop; nop; nop; nop;nop; nop; nop; nop;nop; nop; nop; nop;nop; nop; nop; nop;")

#if CONFIG_HW_CONTROLLER_PCF8574_I2C || CONFIG_HW_CONTROLLER_GPIO || CONFIG_HW_CONTROLLER_NES_PS

int volume, bright;
int inpDelay;
bool shutdown;


bool showMenu;



bool getShowMenu()
{
	return showMenu;
}

//Bit0 Bit1 Bit2 Bit3 Bit4 Bit5 Bit6 Bit7
//SLCT           STRT UP   RGHT DOWN LEFT
//Bit8 Bit9 Bt10 Bt11 Bt12 Bt13 Bt14 Bt15
//L2   R2   L1   R1    /\   O    X   |_|
// NOTE: These mappings aren't reflected in video_audio.c
// any changes have to be reflected in osd_getinput
// TODO: Make these both work together
#define PSX_SELECT 1
#define PSX_START (1 << 3)
#define PSX_UP (1 << 4)
#define PSX_RIGHT (1 << 5)
#define PSX_DOWN (1 << 6)
#define PSX_LEFT (1 << 7)
#define PSX_L2 (1 << 8)
#define PSX_R2 (1 << 9)
#define PSX_L1 (1 << 10)
#define PSX_R1 (1 << 11)
#define PSX_TRIANGLE (1 << 12)
#define PSX_CIRCLE (1 << 13)
#define PSX_X (1 << 14)
#define PSX_SQUARE (1 << 15)
#define A_BUTTON PSX_CIRCLE
#define B_BUTTON PSX_X
#define TURBO_A_BUTTON PSX_TRIANGLE
#define TURBO_B_BUTTON PSX_SQUARE
#define MENU_BUTTON PSX_L1
#define POWER_BUTTON PSX_R1

#if CONFIG_HW_CONTROLLER_PCF8574_I2C
// UP、DOWN、LEFT、RIGHT、A、B、START、SELECT	
int button_info[] = {(1 << 4), (1 << 6), (1 << 7), (1 << 5), (1 << 13), (1 << 14), (1 << 3), (1)};
#endif

#if	CONFIG_HW_CONTROLLER_NES_PS
/*
    A: 		0000 0001 1     (1 << 0)
    B: 		0000 0010 2     (1 << 1)
    start: 	0000 0100 8     (1 << 2)
    select:	0000 1000 4     (1 << 3)
    Up:		0001 0000 16    (1 << 4)
    Down:   0010 0000 32    (1 << 5)
    Left:	0100 0000 64    (1 << 6)
    Right:  1000 0000 128   (1 << 7)
*/
// A、B、START、SELECT、UP、DOWN、LEFT、RIGHT
int sfc_ps_button_info[] = {(1 << 13), (1 << 14), (1), (1 << 3), (1 << 4), (1 << 6), (1 << 7), (1 << 5)};
#endif


bool nes_100ask_is_select_pressed(int ctl)
{
	return !(ctl & PSX_SELECT);
}
bool nes_100ask_is_start_pressed(int ctl)
{
	return !(ctl & PSX_START);
}
bool nes_100ask_is_up_pressed(int ctl)
{
	return !(ctl & PSX_UP);
}
bool nes_100ask_is_right_pressed(int ctl)
{
	return !(ctl & PSX_RIGHT);
}
bool nes_100ask_is_down_pressed(int ctl)
{
	return !(ctl & PSX_DOWN);
}
bool nes_100ask_is_left_pressed(int ctl)
{
	return !(ctl & PSX_LEFT);
}
bool nes_100ask_is_a_pressed(int ctl)
{
	return !(ctl & A_BUTTON);
}
bool nes_100ask_is_b_pressed(int ctl)
{
	return !(ctl & B_BUTTON);
}
bool nes_100ask_is_turbo_a_pressed(int ctl)
{
	return !(ctl & TURBO_A_BUTTON);
}
bool nes_100ask_is_turbo_b_pressed(int ctl)
{
	return !(ctl & TURBO_B_BUTTON);
}
bool nes_100ask_is_menu_pressed(int ctl)
{
	return !(ctl & MENU_BUTTON);
}
bool nes_100ask_is_any_power_pressed(int ctl)
{
	return !(ctl & POWER_BUTTON);
}
bool nes_100ask_is_any_direction_pressed(int ctl)
{
	return nes_100ask_is_up_pressed(ctl) || nes_100ask_is_down_pressed(ctl) || nes_100ask_is_left_pressed(ctl) || nes_100ask_is_right_pressed(ctl);
}

bool nes_100ask_is_any_action_pressed(int ctl)
{
	return nes_100ask_is_start_pressed(ctl) || nes_100ask_is_select_pressed(ctl) || nes_100ask_is_menu_pressed(ctl) || nes_100ask_is_any_power_pressed(ctl);
}

bool nes_100ask_is_any_fire_pressed(int ctl)
{
	return nes_100ask_is_a_pressed(ctl) || nes_100ask_is_b_pressed(ctl) || nes_100ask_is_turbo_a_pressed(ctl) || nes_100ask_is_turbo_b_pressed(ctl);
}

bool is_any_pressed(int ctl)
{
	return nes_100ask_is_any_direction_pressed(ctl) || nes_100ask_is_any_action_pressed(ctl) || nes_100ask_is_any_fire_pressed(ctl);
}

int turboACounter = 0;
int turboBCounter = 0;
int turboASpeed = 3;
int turboBSpeed = 3;
int MAX_TURBO = 6;
int TURBO_COUNTER_RESET = 210;

int nes_100ask_get_turbo_a() {
	return turboASpeed;
}

int nes_100ask_get_turbo_b() {
	return turboBSpeed;
}


static uint8_t i2c_keypad_read()
{
    int len = 1;
    uint8_t *data = malloc(len);

    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, CONFIG_HW_PCF8574_I2C_ADDR << 1 | READ_BIT, ACK_CHECK_EN);
    i2c_master_read_byte(cmd, data + len - 1, NACK_VAL);
    i2c_master_stop(cmd);
    esp_err_t ret = i2c_master_cmd_begin(i2c_port, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);

    uint8_t val = data[0];
    free(data);

    return val;
}


void nes_100ask_joypad_check_delay(uint16_t t)
{
    while(t--);
}


int nes_100ask_controller_read_input()
{
	int b2b1 = 65535;
	if (inpDelay > 0)
		inpDelay--;
#if CONFIG_HW_CONTROLLER_NES_PS
	gpio_set_level(CONFIG_HW_NES_PS_LATCH_PIN, 1);
    nes_100ask_joypad_check_delay(400);
    gpio_set_level(CONFIG_HW_NES_PS_LATCH_PIN, 0);
    for(int i = 0; i < 8; i++)
    {
        if(gpio_get_level(CONFIG_HW_NES_PS_DATA_PIN) == 0)
            b2b1 -= sfc_ps_button_info[i];
        gpio_set_level(CONFIG_HW_NES_PS_CLOCK_PIN, 1);
        nes_100ask_joypad_check_delay(400);
        gpio_set_level(CONFIG_HW_NES_PS_CLOCK_PIN, 0);
        nes_100ask_joypad_check_delay(400);
    }
#elif CONFIG_HW_CONTROLLER_PCF8574_I2C
	uint8_t i2c_data = i2c_keypad_read();
	for (int i = 0; i < 7; ++i)
    {
        if((((1<<i)&i2c_data) == 0))
            b2b1 -= button_info[i];
    }

#elif CONFIG_HW_CONTROLLER_GPIO		
	if (gpio_get_level(CONFIG_HW_GPIO_UP) == 1)
		b2b1 -= PSX_UP;
	if (gpio_get_level(CONFIG_HW_GPIO_DOWN) == 1)
		b2b1 -= PSX_DOWN;
	if (gpio_get_level(CONFIG_HW_GPIO_RIGHT) == 1)
		b2b1 -= PSX_RIGHT;
	if (gpio_get_level(CONFIG_HW_GPIO_LEFT) == 1)
		b2b1 -= PSX_LEFT;
	if (gpio_get_level(CONFIG_HW_GPIO_SELECT) == 1)
		b2b1 -= PSX_SELECT;
	if (gpio_get_level(CONFIG_HW_GPIO_START) == 1)
		b2b1 -= PSX_START;
	if (gpio_get_level(CONFIG_HW_GPIO_B) == 1)
		b2b1 -= B_BUTTON;
	if (gpio_get_level(CONFIG_HW_GPIO_A) == 1)
		b2b1 -= A_BUTTON;
	if (gpio_get_level(CONFIG_HW_GPIO_TURBO_B) == 1)
		b2b1 -= TURBO_B_BUTTON;
	if (gpio_get_level(CONFIG_HW_GPIO_TURBO_A) == 1)
		b2b1 -= TURBO_A_BUTTON;
	if (gpio_get_level(CONFIG_HW_GPIO_MENU) == 1)
		b2b1 -= MENU_BUTTON;
	if (gpio_get_level(CONFIG_HW_GPIO_POWER) == 1)
		b2b1 -= POWER_BUTTON;
#endif
	if (nes_100ask_is_menu_pressed(b2b1) && inpDelay == 0)
	{
		showMenu = !showMenu;
		inpDelay = 20;
	}

	if (showMenu)
	{
		if (inpDelay == 0)
		{
			if (nes_100ask_is_up_pressed(b2b1) && volume < 4)
				volume++;
			if (nes_100ask_is_down_pressed(b2b1) && volume > 0)
				volume--;
			if (nes_100ask_is_right_pressed(b2b1) && bright < 4)
				bright++;
			if (nes_100ask_is_left_pressed(b2b1) && bright > 0)
				bright--;
			if (nes_100ask_is_a_pressed(b2b1))
				nes_100ask_set_y_stretch(1 - nes_100ask_get_y_stretch());
			if (nes_100ask_is_b_pressed(b2b1))
				nes_100ask_set_x_stretch(1 - nes_100ask_get_x_stretch());
			if (nes_100ask_is_turbo_a_pressed(b2b1))
				turboASpeed = (turboASpeed + 1) % MAX_TURBO;
			if (nes_100ask_is_turbo_b_pressed(b2b1))
				turboBSpeed = (turboBSpeed + 1) % MAX_TURBO;
			if (is_any_pressed(b2b1))
				inpDelay = 15;
		}
	}

	if (!showMenu)
	{
		if (turboASpeed > 0 && nes_100ask_is_turbo_a_pressed(b2b1))
		{
			b2b1 |= A_BUTTON;
			if ((turboACounter % (turboASpeed*2)) == 0)
			{
				b2b1 -= A_BUTTON;
			}
			turboACounter = (turboACounter + 1) % TURBO_COUNTER_RESET; // 30 is the LCM of numers 1 thru 6
		}
		else
		{
			turboACounter = 0;
		}

		if (turboBSpeed > 0 && nes_100ask_is_turbo_b_pressed(b2b1))
		{
			b2b1 |= B_BUTTON;
			if ((turboBCounter % (turboBSpeed*2)) == 0)
			{
				b2b1 -= B_BUTTON;
			}
			turboBCounter = (turboBCounter + 1) % TURBO_COUNTER_RESET; // 30 is the LCM of numers 1 thru 6
		}
		else
		{
			turboBCounter = 0;
		}
	}
	return b2b1;
}

int nes_100ask_get_bright()
{
	return bright;
}

int nes_100ask_get_volume()
{
	return volume;
}

bool nes_100ask_get_shutdown()
{
	return shutdown;
}


static esp_err_t i2c_master_driver_initialize()
{
#if CONFIG_HW_CONTROLLER_PCF8574_I2C
    i2c_config_t conf = {
        .mode = I2C_MODE_MASTER,
        .sda_io_num = CONFIG_HW_PCF8574_I2C_SDA,
        .sda_pullup_en = GPIO_PULLUP_ENABLE,
        .scl_io_num = CONFIG_HW_PCF8574_I2C_SCL,
        .scl_pullup_en = GPIO_PULLUP_ENABLE,
        .master.clk_speed = i2c_frequency
    };
    return i2c_param_config(i2c_port, &conf);
#endif
}


static void nes_100ask_ps_controller_init(void)
{
	/* 定义一个gpio配置结构体 */
    gpio_config_t gpio_config_structure;

    /* 初始化gpio配置结构体(CONFIG_HW_NES_PS_CLOCK_PIN) */
    gpio_config_structure.pin_bit_mask = (1ULL << CONFIG_HW_NES_PS_CLOCK_PIN);/* 选择 CONFIG_HW_NES_PS_CLOCK_PIN */
    gpio_config_structure.mode = GPIO_MODE_OUTPUT;              /* 输出模式 */
    gpio_config_structure.pull_up_en = 0;                       /* 不上拉 */
    gpio_config_structure.pull_down_en = 0;                     /* 不下拉 */
    gpio_config_structure.intr_type = GPIO_PIN_INTR_DISABLE;    /* 禁止中断 */ 

    /* 根据设定参数初始化并使能 */  
	gpio_config(&gpio_config_structure);

	/* 初始化gpio配置结构体() */
    gpio_config_structure.pin_bit_mask = (1ULL << CONFIG_HW_NES_PS_LATCH_PIN);/* 选择 CONFIG_HW_NES_PS_LATCH_PIN */
    gpio_config_structure.mode = GPIO_MODE_OUTPUT;              /* 输出模式 */
    gpio_config_structure.pull_up_en = 0;                       /* 不上拉 */
    gpio_config_structure.pull_down_en = 0;                     /* 不下拉 */
    gpio_config_structure.intr_type = GPIO_PIN_INTR_DISABLE;    /* 禁止中断 */ 

    /* 根据设定参数初始化并使能 */  
	gpio_config(&gpio_config_structure);

    /* 初始化gpio配置结构体(CONFIG_HW_NES_PS_DATA_PIN) */
    gpio_config_structure.pin_bit_mask = (1ULL << CONFIG_HW_NES_PS_DATA_PIN);/* 选择 CONFIG_HW_NES_PS_DATA_PIN */
    gpio_config_structure.mode = GPIO_MODE_INPUT;               /* 输入模式 */
    gpio_config_structure.pull_up_en = 0;                       /* 不上拉 */
    gpio_config_structure.pull_down_en = 0;                     /* 不下拉 */
    gpio_config_structure.intr_type = GPIO_PIN_INTR_DISABLE;    /* 禁止中断 */ 

    /* 根据设定参数初始化并使能 */  
	gpio_config(&gpio_config_structure);
}


void nes_100ask_controller_init()
{
	printf("Game controller initalizing\n");
#if CONFIG_HW_CONTROLLER_NES_PS
	printf("CONFIG_HW_CONTROLLER_NES_PS\n");
	nes_100ask_ps_controller_init();
	//gpio_set_direction(CONFIG_HW_NES_PS_CLOCK_PIN, GPIO_MODE_OUTPUT);
	//gpio_set_direction(CONFIG_HW_NES_PS_LATCH_PIN, GPIO_MODE_OUTPUT);
	//gpio_set_direction(CONFIG_HW_NES_PS_DATA_PIN, GPIO_MODE_INPUT);
	printf("SFC PS Control initated\n");
#elif CONFIG_HW_CONTROLLER_PCF8574_I2C
	printf("CONFIG_HW_CONTROLLER_PCF8574_I2C\n");
	i2c_master_driver_initialize();
	//i2c_driver_install(i2c_port, I2C_MODE_MASTER, I2C_MASTER_RX_BUF_DISABLE, I2C_MASTER_TX_BUF_DISABLE, 0);
	i2c_driver_install(i2c_port, I2C_MODE_MASTER, 0, 0, 0);
	printf("PCF8574 I2C Control initated\n");
#elif CONFIG_HW_CONTROLLER_GPIO
	printf("CONFIG_HW_CONTROLLER_GPIO\n");
	nes_100ask_init_gpio(CONFIG_HW_GPIO_START);
	nes_100ask_init_gpio(CONFIG_HW_GPIO_SELECT);
	nes_100ask_init_gpio(CONFIG_HW_GPIO_UP);
	nes_100ask_init_gpio(CONFIG_HW_GPIO_DOWN);
	nes_100ask_init_gpio(CONFIG_HW_GPIO_LEFT);
	nes_100ask_init_gpio(CONFIG_HW_GPIO_RIGHT);
	nes_100ask_init_gpio(CONFIG_HW_GPIO_B);
	nes_100ask_init_gpio(CONFIG_HW_GPIO_A);
	nes_100ask_init_gpio(CONFIG_HW_GPIO_TURBO_B);
	nes_100ask_init_gpio(CONFIG_HW_GPIO_TURBO_A);
	nes_100ask_init_gpio(CONFIG_HW_GPIO_MENU);
	nes_100ask_init_gpio(CONFIG_HW_GPIO_POWER);
	printf("GPIO Control initated\n");
#endif
	inpDelay = 0;
	volume = 4;
	bright = 2;
}

#else

int nes_100ask_controller_read_input()
{
	return 0xFFFF;
}

void nes_100ask_controller_init()
{
	printf("PSX controller disabled in menuconfig; no input enabled.\n");
}

#endif