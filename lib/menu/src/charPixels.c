#include "freertos/FreeRTOS.h"
#include "menu.h"
#include "esp_system.h"
#include "esp_event.h"
#include "esp_event_loop.h"
#include "esp_partition.h"
#include "nvs_flash.h"
#include "charData.c"
#include <string.h>
#include "iconData.c"
#include "pretty_effect.h"

#include <sys/types.h>
#include<dirent.h>

int yLineEnded;
int charOff;
int change;

bool cpGetPixel(char cpChar, int cp1, int cp2)
{
	return getPixel(cpChar, cp1, cp2);
}

//Load Rom list from flash partition to char array(lines), init some variables for printing rom list
void initRomList()
{
	DIR *pDir = NULL;
	struct dirent * pEnt = NULL;
	pDir = opendir("/sdcard/nes");
	if (NULL == pDir)
	{
		perror("opendir");
	}

	char fileName[FILENAME_LENGTH][FILENAME_LENGTH+1];
	entryCount = -1;
	int dir_count = 0;
	while (1)
	{
		pEnt = readdir(pDir);
		if(pEnt != NULL)
		{
			printf("name：[%s]	\n", pEnt->d_name);
			strcpy(fileName[dir_count], pEnt->d_name);
			dir_count++;
			entryCount++;
		}
		else
		{
			break;
		}
	}
	closedir(pDir);

	menuEntries = (MenuEntry *)malloc(entryCount * sizeof(MenuEntry));
	for (int i = 0; i < entryCount; i++)
	{
		//menuEntries[i].entryNumber = i;
		//menuEntries[i].icon = 'E';
		menuEntries[i].icon = '$';
		//strcpy(menuEntries[i].name, fileName[i]);
		strcpy(menuEntries[i].fileName, fileName[i]);

		for (int j = FILENAME_LENGTH; j > 0; j--) 
		{
			if (menuEntries[i].fileName[j] < ' ') {
				menuEntries[i].fileName[j] = '\0';
			}
		}

	}
	printf("Read %d rom entries\n", entryCount);
}

//get depending on x/y value the actual char from lines array
//depending on x/y/actChar return color/black if char value is true/false(charData.c)
//depending on x/y/actChar read and return icon pixel color(iconData.c)
int nes_100ask_get_char_pixel(int x, int y, int change, int choosen)
{
	int page = choosen / 13;
	int line = ((y - 3) / 18) + 13 * page;
	int charNo = (x - 26) / 16;
	char actChar;

	if (line >= entryCount)
	{
		return 0x0000;
	}
	if (x >= 7 && x <= 22)
	{
		if (line == choosen)
			return getIconPixel(menuEntries[line].icon, x - 7, (y - 5) % 18, change);
		else
			return 0x0000;
	}
	//actChar = menuEntries[line].name[charNo];
	actChar = menuEntries[line].fileName[charNo];
	if (actChar < ' ')
	{
		return 0x0000;
	}
	if (getPixel(actChar, (x - 26) % 16, (y - 3) % 18) == 1)
		return 0x001F;
	return 0x0000;
}

void nes_100ask_free_rl()
{
	free(menuEntries);
}